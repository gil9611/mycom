package com.test.mycom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycomApplication {

	public static void main(String[] args) {
		SpringApplication.run(MycomApplication.class, args);
	}

}
